## full_oppo6771_17065-user 9 PPR1.180610.011 eng.root.20200509.060522 release-keys
- Manufacturer: oppo
- Platform: 
- Codename: CPH1859
- Brand: google
- Flavor: dot_CPH1859-userdebug
- Release Version: 11
- Id: RQ2A.210505.002
- Incremental: eng.ghost.20210522.131811
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: google/flame/flame:11/RQ2A.210505.002/7246365:user/release-keys
- OTA version: 
- Branch: full_oppo6771_17065-user-9-PPR1.180610.011-eng.root.20200509.060522-release-keys
- Repo: google_cph1859_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
